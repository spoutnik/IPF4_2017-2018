let exp = Mult( Int(3), Plus( Plus( Var("sigma"), Int(1) ), Var("sigma") ) );;
expr_to_string exp;;
variables exp;;
nbe_occurences exp "sigma";;
let exp2 = substitute exp "sigma" (Int(8));;
expr_to_string exp2;;
simplify exp2;;

let expb = If(Int(0), Var("True"), Var("False"));;
translator expb;;
expr_to_string expb;;

let exp3 = Soit("test", Var("non"), Plus(Var("moinsun"), Plus(Soit("test2",Var("oui"),Var("test2")),Var("test"))));;
expr_to_string exp3;;
variables exp3;;
expr_to_string (eval_nom exp3 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Var("moinsun"))]);;
expr_to_string (eval_val exp3 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Var("moinsun"))]);;
simplify(eval_val exp3 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;

let exp4 = If(exp3, Var("True"), Var("False"));;
eval_nom exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))];;
expr_to_string (eval_nom exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;

let suite = Soit (
    "d",
    Int(3),
    Mult (
        Plus (
            Var("a"),
            Var("c")
        ),
        Plus (
            Int(3),
            Var("d")
        )
    )
);;

let nana = Soit (
    "a",
    If (
        Mult (
            Var ("a"),
            Var ("d")
        ),
        Mult (
            Int (-1),
            Plus (
                Var ("c"),
                Int (-2)
            )
        ),
        Mult (
            Int (-1),
            Var ("c")
        )
    ),
    suite
);;

let test = eval_val nana [("d", Int(1)); ("c", Int(6)); ("a", Int(4))];;
expr_to_string test;;
simplify test;;
translator nana;;
