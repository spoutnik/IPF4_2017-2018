type expr =
    | Var of string
    | Int of int
    | Plus of expr * expr
    | Mult of expr * expr
    | If of expr * expr * expr
    | Soit of string * expr * expr;;

(*************************************************************)

(** substitute
 * @param exp x v: expr -> string -> expr
 * @pre: l'expression ne contient pas de `Soit`
 * @return: e où toutes les occurences de x ont été remplacées par v
 *)

let rec substitute exp x v =  match exp with
    | Var va -> if (va = x) then v else Var(va)
    | Int i -> Int i
    | Plus(e1, e2) -> Plus((substitute e1 x v), (substitute e2 x v))
    | Mult(e1, e2) -> Mult((substitute e1 x v), (substitute e2 x v))
    | If(e, ev, ef) -> If(substitute e x v, substitute ev x v, substitute ef x v)
    | Soit(x, v, e) -> failwith "subsitute: Soit clauses not supported";;

(** meaning
 * @param exp env: expr -> string*expr list
 * @return: exp traduit pour l'environnement env
 *)

let rec meaning exp env = match env with
    | [] -> exp
    | (x, v)::q -> meaning (substitute exp x v) q;;

(** eval_val
 * @param exp env: expr -> string*expr list
 * @return: exp evaluée pour l'environnement env, en employant la stratégie par valeur
 *)

let eval_val exp env = 
    let rec aux exp env = match exp with
        | Var va -> Var(va)
        | Int i -> Int(i)
        | Plus(e1, e2) -> Plus((aux e1 env), (aux e2 env))
        | Mult(e1, e2) -> Mult((aux e1 env), (aux e2 env))
        | If(e, ev, ef) -> If(aux e env, aux ev env, aux ef env)
        | Soit(x, v, e) -> let new_env = (x, meaning (aux v env) env)::env in meaning (aux e new_env) new_env
    in
    meaning (aux exp env) env;;

(** eval_nom
 * @param exp env: expr -> string*expr list
 * @return: exp evaluée pour l'environnement env, en employant la stratégie par nom
 *)

let eval_nom exp env = 
    let rec aux exp env = match exp with
        | Var va -> Var(va)
        | Int i -> Int(i)
        | Plus(e1, e2) -> Plus((aux e1 env), (aux e2 env))
        | Mult(e1, e2) -> Mult((aux e1 env), (aux e2 env))
        | If(e, ev, ef) -> If(aux e env, aux ev env, aux ef env)
        | Soit(x, v, e) -> let new_env = (x, v)::env in meaning (aux e new_env) new_env
    in
    meaning (aux exp env) env;;

(** translator
 * @param exp: expr
 * @pre: l'expression ne contient pas de `Soit`
 * @return: exp où les composantes If ont été traduites (en ev ou ef, suivant e)
 *)

let rec translator exp =
    let rec simplify_local ex = match ex with
        | Var v -> failwith (String.concat "" ["translator: Unbound Variable: "; v])
        | Int i -> i
        | Plus(e1, e2) -> (simplify_local e1) + (simplify_local e2)
        | Mult(e1, e2) -> (simplify_local e1) * (simplify_local e2)
        | If(e, ev, ef) -> simplify_local (translator(If(e, ev, ef)))
        | Soit(x, v, e) -> simplify_local (eval_nom (Soit(x, v, e)) [])
    in
    match exp with
        | If(e, ev, ef) -> if (simplify_local e) <> 0 then ev else ef
        | exp -> exp;;

(** invert
 * @param l: a' list
 * @return: l inversée
 *)

let invert l =
    let rec aux l buffer = match l with
        | [] -> buffer
        | h::q -> aux q (h::buffer)
    in
    aux l [];;

(** expr_to_string
 * @param exp: expr
 * @pre: l'expression ne contient pas de `Soit`
 * @return: une string exprimant exp de façon lisible pour l'utilisateur
 *)

let expr_to_string exp =
    let rec aux exp l = match exp with
        | Var v -> v::l
        | Int i -> (string_of_int i)::l
        | Plus(e1, e2) -> let l = "("::l in ")"::(aux e2 ("+"::(aux e1 l)))
        | Mult(e1, e2) -> aux e2 ("x"::(aux e1 l))
        | If(e, ev, ef) -> aux (translator (If(e, ev, ef))) l
        | Soit(x, v, e) -> aux (eval_nom (Soit(x, v, e)) []) l
    in
    String.concat " " (invert (aux exp []));;
    
(** is_in
 * @param x l: a' -> a' list
 * @return: booléen codant si x est dans l
 *)

let rec is_in x l = match l with
    | [] -> false
    | h::q -> if (h = x) then true else is_in x q;;

(** concat
 * @param l1 l2: a' list -> a' list
 * @return: l1 U l2
 *)

let rec concat l1 l2 = match l1 with
    | [] -> l2
    | h::q -> if (is_in h l2) then concat q l2 else concat q (h::l2);;

(** variables
 * @param exp: expr
 * @pre: l'expression ne contient pas de `Soit`
 * @return: la liste des variables présentes dans exp
 *)

let variables exp =
    let rec aux ex l = match ex with
        | Var v -> v::l
        | Int i -> l
        | Plus(e1, e2) -> concat (aux e1 l) (aux e2 l)
        | Mult(e1, e2) -> concat (aux e1 l) (aux e2 l)
        | If(e, ev, ef) -> aux (translator(If(e, ev, ef))) l
        | Soit(x, v, e) -> aux (eval_nom (Soit(x, v, e)) []) l
    in
    aux exp [];;

(** simplify
 * @param exp: expr
 * @pre: l'expression ne contient pas de `Soit`; exp ne contient pas de variables
 * @return: un entier correspondant à l'évaluation de exp
 *)

let rec simplify exp = match exp with
    | Var v -> failwith (String.concat "" ["simplify: Unbound Variable: "; v])
    | Int i -> i
    | Plus(e1, e2) -> (simplify e1) + (simplify e2)
    | Mult(e1, e2) -> (simplify e1) * (simplify e2)
    | If(e, ev, ef) -> simplify (translator(If(e, ev, ef)))
    | Soit(x, v, e) -> simplify (eval_nom (Soit(x, v, e)) []);;

(** nbe_occurence
 * @param exp x: expr -> string
 * @pre: l'expression ne contient pas de `Soit`
 * @return: le nombre d'occurences de x dans e
 *)

let rec nbe_occurences exp x = match exp with
    | Var v -> if (x = v) then 1 else 0
    | Int i -> 0
    | Plus(e1, e2) -> (nbe_occurences e1 x) + (nbe_occurences e2 x)
    | Mult(e1, e2) -> (nbe_occurences e1 x) + (nbe_occurences e2 x)
    | If(e, ev, ef) -> nbe_occurences (translator(If(e, ev, ef))) x
    | Soit(x, v, e) -> nbe_occurences (eval_nom (Soit(x, v, e)) []) x;;

let exp = Mult( Int(3), Plus( Plus( Var("sigma"), Int(1) ), Var("sigma") ) );;
expr_to_string exp;;
variables exp;;
nbe_occurences exp "sigma";;
let exp2 = substitute exp "sigma" (Int(8));;
expr_to_string exp2;;
simplify exp2;;

let expb = If(Int(0), Var("True"), Var("False"));;
translator expb;;
expr_to_string expb;;

let exp3 = Soit("test", Var("non"), Plus(Var("moinsun"), Plus(Soit("test2",Var("oui"),Var("test2")),Var("test"))));;
expr_to_string exp3;;
variables exp3;;
expr_to_string (eval_nom exp3 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Var("moinsun"))]);;
expr_to_string (eval_val exp3 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Var("moinsun"))]);;
simplify(eval_val exp3 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;

let exp4 = If(exp3, Var("True"), Var("False"));;
eval_nom exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))];;
expr_to_string (eval_nom exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;

let suite = Soit (
    "d",
    Int(3),
    Mult (
        Plus (
            Var("a"),
            Var("c")
        ),
        Plus (
            Int(3),
            Var("d")
        )
    )
);;

let nana = Soit (
    "a",
    If (
        Mult (
            Var ("a"),
            Var ("d")
        ),
        Mult (
            Int (-1),
            Plus (
                Var ("c"),
                Int (-2)
            )
        ),
        Mult (
            Int (-1),
            Var ("c")
        )
    ),
    suite
);;

let test = eval_val nana [("d", Int(1)); ("c", Int(6)); ("a", Int(4))];;
expr_to_string test;;
simplify test;;
translator nana;;
