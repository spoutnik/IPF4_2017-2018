# Projet IPF - Groupe 4 - Jean-Baptiste Skutnik

## Introduction

Le sujet du groupe 4 consiste en l'implémentation d'expressions arithmétiques à l'aide d'arbres, de définition:

```ocaml
type expr =
    | Var of string
    | Int of int
    | Plus of expr * expr
    | Mult of expr * expr
    | If of expr * expr * expr
    | Soit of string * expr * expr;;
```

Ainsi dans la suite du rapport, je ferai référence aux arbres en parlant de cette structure de données. De plus, je commenterai les fonctions pour le type `expr` tel qu'il devient à la fin du sujet, c'est à dire avec des clauses `If` et `Soit`.

Ce rapport présente question par question les implémentations obtenues.

## 
### 1. `expr_to_string`

Pour créer une chaîne de caractères à partir d'une variable `expr`, j'ai décidé de __parcourir cette dernière en ajoutant à une pile les éléments rencontrés__, dans l'ordre.
La difficulté se trouvait dans le fait que des parenthèses doivent être introduites dans la forme infixe pour ne pas changer le sens de l'expression:

```ocaml
let aux exp l = match exp with
   ...
    | Plus(e1, e2) -> let l = "("::l in ")"::(aux e2 ("+"::(aux e1 l)))
   ...
```

Les parenthèses sont ajoutées dans le sens inverse: une fois la pile terminée, il faut l'__inverser pour afficher son contenu dans le sens correspondant à sa définition__: d'où la fonction `invert`. J'utilise alors `String.concat` de la bibliothèque standard pour créer une string à partir de la pile, en insérant des espaces entre chaque élément.

La fonction à été adaptée et fait appel à `translator` pour interpréter les clauses `If`. Dans le cas d'une clause `Soit`, l'expression est évaluée avec `eval_nom` pour ensuite être traitée normalement.

## 
### 2. `variables`

Afin de sortir la liste des variables apparaissant dans une `expr`, il suffit d'ajouter à une liste chaque variable rencontrée. __Lors d'une bifurcation__ à cause d'un `Plus` ou `Mult`, __on exécute la fonction sur les deux parties et concatène les listes qui en résultent__. Les fonctions annexes `concat` et `is_in` assurent cette fusion de listes, en omettant les entrées multiples.

La fonction à été adaptée et fait appel à `translator` pour interpréter les clauses `If`. Dans le cas d'une clause `Soit`, l'expression est évaluée avec `eval_nom` pour ensuite être traitée normalement.

## 
### 3. `simplify`

`simplify` va simplement effectuer les opérations décrites par l'arbre représentant l'expression et renvoyer le résultat.

La fonction à été adaptée et fait appel à `translator` pour interpréter les clauses `If`. Dans le cas d'une clause `Soit`, l'expression est évaluée avec `eval_nom` pour ensuite être traitée normalement.

## 
### 4. `nbe_occurences`

Pour compter le nombre d'occurences d'une variable, il suffit d'atteindre chaque feuille, et de renvoyer `1` si l'élément considéré est la variable recherchée, `0` sinon; en additionnant les retours pour chaque branche, on obtient le nombre d'occurences de cette variable.

La fonction à été adaptée et fait appel à `translator` pour interpréter les clauses `If`. Dans le cas d'une clause `Soit`, l'expression est évaluée avec `eval_nom` pour ensuite être traitée normalement.

## 
### 5. `substitute`

Substituer revient à créer un nouvel arbre en remplaçant chaque occurence d'un certain élément. En parcourant l'arbre, `substitute exp x v` va considérer chaque élément de `exp` et renvoyer:

- `v` si cet élément est `x`;
- le même élément sinon.

En l'appelant récursivement sur l'arbre, on construit un nouvel arbre où toutes les occurences de `x` sont remplacées par `v`.

La fonction à été adaptée et fait appel à `translator` pour interpréter les clauses `If`, et s'arrête avec `Not supported` si elle rencontre un `Soit` dans l'expression.

Cette fonction soulève une exception à l'encontre d'une clause `Soit`: elle est faite pour être utilisée après `eval_nom` ou `eval_val`.

## 
### 6. `translator`

Afin de pouvoir comprendre une clause `If`, il faut pouvoir évaluer une expression, qui peut contenir une clause `If` ... Ainsi	`simlplify` n'est pas adaptée à la résolution de ces clauses.  
Pour y remédier, j'ai créé `translator`, avec une fonction annexe `simplify_local`, qui ne sera utilisée que pour simplifier la clause `If` à évaluer. Une fois sa valeur obtenue, `translator` renverra `ev` ou `ef` suivant le résultat.

J'ai préféré créer cette fonction contenant une copie de `simplify` pour pouvoir par la suite l'utiliser pour adapter toutes les autres fonctions, qui gagnent une clause `If` de type:

```ocaml
let variables ex =
    let rec aux ex l = match ex with
       ...
        | If(e, ev, ef) -> aux (translator(If(e, ev, ef))) l
       ...
    in
    aux ex [];;
```

Où `translator` retourne l'expression à considérer.

## 
### 7. `eval_val` et `eval_nom`

Afin d'évaluer des expressions, j'ai suivi les stratégies données par le sujet, en utilisant les fonctions précédentes.

Cependant, j'ai d'abord choisi d'implémenter `meaning`, qui a pour effet d'appeler `substitute` pour chaque variable de l'environnement.

`eval_val` et `eval_nom` n'ont alors qu'à traduire les expressions passées en paramètres en enlevant les clauses `Soit` en suivant leur stratégie respective, puis à appeler `meaning` sur l'expression résultante pour obtenir une expression évaluée pour tout l'environnement.

# Annexe

## Interfaces

```ocaml
Question 1

(** invert
 * @param l: a' list
 * @return: l inversée
 *)

(** expr_to_string
 * @param exp: expr
 * @return: une string exprimant exp de façon lisible pour l'utilisateur
 *)

Question 2

(** is_in
 * @param x l: a' -> a' list
 * @return: booléen codant si x est dans l
 *)

(** concat
 * @param l1 l2: a' list -> a' list
 * @return: l1 U l2
 *)

(** variables
 * @param exp: expr
 * @return: la liste des variables présentes dans ex
 *)

Question 3

(** simplify
 * @param exp: expr
 * @pre: exp ne contient pas de variables
 * @return: un entier correspondant à l'évaluation de exp
 *)

Question 4

(** nbe_occurence
 * @param exp x: expr -> string
 * @return: le nombre d'occurences de x dans e
 *)

Question 5

(** substitute
 * @param exp x v: expr -> string -> expr
 * @pre: l'expression ne contient pas de `Soit`
 * @return: e où toutes les occurences de x ont été remplacées par v
 *)

Question 6

(** translator
 * @param exp: expr
 * @return: exp où les composantes If ont été traduites (en ev ou ef, suivant e)
 *)

Question 7

(** meaning
 * @param exp env: expr -> (string * expr) list
 * @return: exp traduit pour l'environnement env
 *)

(** eval_val
 * @param exp env: expr -> (string * expr) list
 * @return: exp evaluée pour l'environnement env, en employant la stratégie par valeur
 *)

(** eval_nom
 * @param exp env: expr -> (string * expr) list
 * @return: exp evaluée pour l'environnement env, en employant la stratégie par nom
 *)
```

## Tests

```ocaml
(* Tests sur les premieres fonctions (liste non exhaustive) *)
let exp = Mult( Int(3), Plus( Plus( Var("sigma"), Int(1) ), Var("sigma") ) );;
expr_to_string exp;;
variables exp;;
nbe_occurences exp "sigma";;
let exp2 = substitute exp "sigma" (Int(8));;
expr_to_string exp2;;
simplify exp2;;

(* Tests sur les If *)
let exp3 = If(exp3, Var("True"), Var("False"));;

let exp4 = Soit(
    "test", 
    Var("non"), 
    Plus(
	Var("moinsun"),
	Plus(
	    Soit(
		"test2",
		Var("oui"),
		Var("test2")
	    ),
	    Var("test")
	) 
    ) 
);;
expr_to_string (eval_nom exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;
expr_to_string (eval_val exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;
simplify(eval_val exp4 [("moinsun", Int(-1)); ("oui", Int(2)); ("non", Int(-1))]);;
```
